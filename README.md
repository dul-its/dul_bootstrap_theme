# DUL Bootstrap Theme
A shared, re-usable Duke University Libraries theme, including styles, scripts, and templates for use in other Rails applications at DUL. This theme provides markup for a shared DUL masthead and footer, Bootstrap-based interface elements, and a DUL color scheme.

## Example Applications
- [DUL QuickSearch](https://gitlab.oit.duke.edu/dul-its/dul-quicksearch)
- [DUL Staff Directory](https://gitlab.oit.duke.edu/dul-its/dul-directory-admin)

## Usage
In a Rails app that will use this theme, add the following to your `Gemfile` (use whichever tag is appropriate):
```
gem 'dul_bootstrap_theme',
    git: 'https://gitlab.oit.duke.edu/dul-its/dul_bootstrap_theme/',
    tag: 'v1.0.0'
```

## Updates
To create new releases of this engine, update the `VERSION` in `version.yml`. Upon a push to `main`, this will automatically create a corresponding git tag.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
