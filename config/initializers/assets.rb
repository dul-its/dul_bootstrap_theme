# This makes the engine's assets available to the host app
# See https://github.com/rails/sprockets/issues/542

Rails.application.config.assets.precompile << "dul_bootstrap_theme_manifest.js"
