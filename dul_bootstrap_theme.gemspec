# frozen_string_literal: true

require_relative 'lib/dul_bootstrap_theme/version'

Gem::Specification.new do |spec|
  spec.name        = 'dul_bootstrap_theme'
  spec.version     = DulBootstrapTheme::VERSION
  spec.authors     = ['Sean Aery']
  spec.email       = ['sean.aery@duke.edu']
  spec.homepage    = 'https://library.duke.edu'
  spec.summary     = 'A Bootstrap-based theme for DUL applications.'
  spec.description = 'A Bootstrap-based theme for DUL applications.'
  spec.license     = 'MIT'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://library.duke.edu'
  spec.metadata['changelog_uri'] = 'https://library.duke.edu'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '~> 7.2'

  spec.add_dependency 'bootstrap', '~> 4.6.2.1'
  spec.add_dependency 'jquery-rails'
  spec.add_dependency 'font-awesome-sass', '~> 5.15.1'
end
