# frozen_string_literal: true

require 'bootstrap'
require 'jquery-rails'
require 'font-awesome-sass'

module DulBootstrapTheme
  class Engine < ::Rails::Engine
    isolate_namespace DulBootstrapTheme
  end
end
