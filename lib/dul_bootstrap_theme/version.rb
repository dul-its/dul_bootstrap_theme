require 'yaml'

module DulBootstrapTheme
  VERSION = YAML.load_file(File.expand_path('../../version.yml', __dir__))['variables']['VERSION']
end
